/*Project aria2-WIOW
 *
 * Copyright (c) 2014 - 2023, multiSnow <https://git.disroot.org/multiSnow>
 *
 *Permission to use, copy, modify, and/or distribute this software for
 *any purpose with or without fee is hereby granted, provided that the
 *above copyright notice and this permission notice appear in all
 *copies.
 *
 *THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 *WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 *AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 *DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 *PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 *TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *PERFORMANCE OF THIS SOFTWARE.
 */

function connect(){
    let nsidetags=document.getElementById('sidetags');
    let nwsaddr=document.getElementById('ws_address');
    let nwshost=document.getElementById('wshost');
    let nwsport=document.getElementById('wsport');
    let nrpctoken=document.getElementById('rpctoken');

    let nsideinfodyn=document.getElementById('sideinfo_dynamic');
    let nmain=document.getElementById('main');

    let wss_scheme=document.getElementById('wss_scheme').checked===true?'wss://':'ws://';
    let url=[wss_scheme,nwshost.value,':',nwsport.value,'/jsonrpc'].join('');
    let autorefresh=undefined;

    ws=new WebSocket(url);
    ws.onerror=function(message){
        alert('Failed to open '+ws.url)
        return 0;
    };

    ws.onclose=function(message){
        clearInterval(autorefresh);
        let idl=['mainactive','mainstopped','mainwaiting'];
        for(let n of idl){
            document.getElementById(n).innerHTML='';
        };
        nwshost.value=popattr(nwsaddr,'data-wshost');
        nwsport.value=popattr(nwsaddr,'data-wsport');
        nrpctoken.value=popattr(nwsaddr,'data-rpctoken');
        nwshost.disabled=false;
        nwsport.disabled=false;
        nrpctoken.disabled=false;
        topage();
        hidenode(nsidetags);
        hidenode(nmain);
        document.getElementById('sideinfo_static').innerHTML='Disconnected!';
        hidenode(nsideinfodyn);
        hidenode(document.getElementById('shutdown'));
        hidenode(document.getElementById('disconnect'));
        shownode(nwsaddr);
        shownode(document.getElementById('connect'));
        document.title='aria2 WIOW';
        if(message.code!==1005){
            alert(message.code+' '+message.reason+' '+message.wasClean);
        };
        return 0;
    };

    ws.onmessage=function(message){
        let idfunc_dict={'mainactive':showactive,'mainstopped':showstopped,'mainwaiting':showwaiting};
        let showtagname=getattr(nsidetags,'data-crtshow');
        let msg_data=JSON.parse(message.data);
        if('id' in msg_data){
            if('error' in msg_data){
                notification(msg_data);
            }else{
                try{
                    func[msg_data.id](msg_data);
                }catch(e){
                    console.log(msg_data.id,e);
                };
            };
        }else{
            if(msg_data.method in func){
                func[msg_data.method](msg_data);
                if(showtagname in idfunc_dict){
                    idfunc_dict[showtagname]();
                };
            };
        };
        return 0;
    };

    ws.onopen=function(message){
        nwshost.disabled=true;
        nwsport.disabled=true;
        nrpctoken.disabled=true;
        setattr(nwsaddr,'data-wshost',nwshost.value);
        setattr(nwsaddr,'data-wsport',nwsport.value);
        setattr(nwsaddr,'data-rpctoken',nrpctoken.value);
        location.hash='',
        shownode(nsideinfodyn);
        shownode(nsidetags);
        shownode(document.getElementById('shutdown'));
        shownode(nmain);
        getversion();topage('start');switch_add_type();default_option();
        shownode(document.getElementById('disconnect'));
        hidenode(nwsaddr);
        hidenode(document.getElementById('connect'));
        autorefresh=start_autorefresh()
        return 0;
    };
    return 0;
};

function disconnect(){
    ws.close();
    return 0;
};

function wsreq(id,method,params){
    if(ws.readyState!==1){
        alert('Connection lost or not yet open!');
        return 1;
    };
    let json={jsonrpc:'2.0',id:id,method:method,
              params:(typeof params===typeof undefined)?[]:params};
    let rpctoken=getattr(document.getElementById('ws_address'),'data-rpctoken');
    if(rpctoken)json.params.unshift(`token:${rpctoken}`);
    ws.send(JSON.stringify(json));
    return 0;
};
