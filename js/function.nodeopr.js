/*Project aria2-WIOW
 *
 * Copyright (c) 2014 - 2023, multiSnow <https://git.disroot.org/multiSnow>
 *
 *Permission to use, copy, modify, and/or distribute this software for
 *any purpose with or without fee is hereby granted, provided that the
 *above copyright notice and this permission notice appear in all
 *copies.
 *
 *THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 *WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 *AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 *DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 *PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 *TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *PERFORMANCE OF THIS SOFTWARE.
 */

function opr_pop(gid){
    let node=document.getElementById(gid);
    if(node)node.remove();
    return 0;
};

function create_itemnode(gid,cls,type){
    let atpmap={'active':'a','waiting':'w','stopped':''};
    let atsmap={'http':'http','torrent':'btml','metalink':'btml','stop':'stop'};
    let node=newtag('div');
    node.id=gid;
    node.classList.add('item',`item_${cls}`);
    setattr(node,'data-artype',`${atpmap[type]}${atsmap[cls]}`);

    let item_gid=newtag('div',node);
    let item_title=newtag('div',node);
    let item_button=newtag('div',node);
    let item_summery=newtag('div',node);
    if(type!='stopped'){
        let pause_icon=newtag('canvas',item_button);
        pause_icon.id='pause_icon';
        if(type=='active'){
            pause_icon.textContent='pause';
            setattr(pause_icon,'onclick',`pause('${gid}')`);
            pausecanvas(pause_icon);
        }else if(type=='waiting'){
            pause_icon.textContent='unpause';
            setattr(pause_icon,'onclick',`unpause('${gid}')`);
            unpausecanvas(pause_icon);
        }
    }
    let remove_icon=newtag('canvas',item_button);
    let option_icon=newtag('canvas',item_button);
    let infohash_div=newtag('div',item_summery);
    let progress_bar=newtag('progress',item_summery);
    let progress=newtag('div',item_summery);
    if(type!='stopped'){
        let spddiv=newtag('div',item_summery);
        let dspd=newtag('div',spddiv);
        let uspd=newtag('div',spddiv);
        let eta=newtag('div',spddiv);
        let connection=newtag('div',item_summery);
        spddiv.classList.add('flexbox');
        dspd.id='dspd';
        dspd.classList.add('item_spd');
        uspd.id='uspd';
        uspd.classList.add('item_spd');
        eta.id='eta';
        connection.id='connection';
    }

    item_gid.id='item_gid';
    item_gid.className='item_gid';
    item_title.id='item_title';
    item_title.className='item_title';
    item_button.className='item_button';
    remove_icon.id='remove_icon';
    remove_icon.textContent='remove';
    if(type=='stopped'){
        setattr(remove_icon,'onclick',`remove_stopped('${gid}')`);
    }else{
        setattr(remove_icon,'onclick',`remove_active('${gid}')`);
    }
    option_icon.id='option_icon';
    option_icon.textContent='status&option';
    setattr(option_icon,'onclick',`showoption('${gid}')`);
    item_summery.className='item_summery';
    if(type!='stopped'){
        infohash_div.id='infohash';
        infohash_div.className='infohash';
    }
    progress_bar.className='mainprogress';
    progress.id='progress_text';

    removecanvas(remove_icon);
    optioncanvas(option_icon);
    return node;
};

function opr_active(gid,dict){
    let completedLength=dict['completedLength'];
    let totalLength=dict['totalLength'];
    let downloadSpeed=dict['downloadSpeed'];
    let root=document.getElementById('mainactive');
    let node=document.getElementById(gid);
    let dictmap={
        'item_gid':gid,
        'item_title':dict['name'],
        'infohash':dict['infohash'],
        'progress_text':`${percentage(completedLength,totalLength)} of ${human_read(totalLength)}`,
        'dspd':`D: ${human_read(downloadSpeed)}/s`,
        'uspd':`U: ${human_read(dict['uploadSpeed'])}/s`,
        'eta':`ETA: ${spendtime(downloadSpeed,completedLength,totalLength)}`,
        'connection':`download from ${dict['connections']} connection.`,
    };
    if(!node){
        node=create_itemnode(gid,dict['cls'],'active');
        root.insertBefore(node,root.children[dict['i']]);
    }else{
        let rect=node.getBoundingClientRect();
        if(rect.top>window.innerHeight||rect.top+rect.height<0)
            return 0;
    };
    for(let n of node.getElementsByTagName('div')){
        if(n.id in dictmap){
            n.textContent=dictmap[n.id];
        };
    };
    let progress_bar=node.getElementsByTagName('progress')[0];
    progress_bar.value=completedLength;
    progress_bar.max=totalLength;
    return 0;
};

function opr_stopped(gid,dict){
    let completedLength=dict['completedLength'];
    let totalLength=dict['totalLength'];
    let root=document.getElementById('mainstopped');
    let node=document.getElementById(gid);
    let dictmap={
        'item_gid':gid,
        'item_title':dict['name'],
        'progress_text':`${percentage(completedLength,totalLength)} of ${human_read(totalLength)}`,
    };
    if(!node){
        node=create_itemnode(gid,'stop','stopped');
        root.insertBefore(node,root.children[dict['i']]);
    }else{
        let rect=node.getBoundingClientRect();
        if(rect.top>window.innerHeight||rect.top+rect.height<0)
            return 0;
    };
    for(let n of node.getElementsByTagName('div')){
        if(n.id in dictmap){
            n.textContent=dictmap[n.id];
        };
    };
    let progress_bar=node.getElementsByTagName('progress')[0];
    progress_bar.value=completedLength;
    progress_bar.max=totalLength;
    return 0;
};

function opr_waiting(gid,dict){
    let completedLength=dict['completedLength'];
    let totalLength=dict['totalLength'];
    let downloadSpeed=dict['downloadSpeed'];
    let root=document.getElementById('mainwaiting');
    let node=document.getElementById(gid);
    let dictmap={
        'item_gid':gid,
        'item_title':dict['name'],
        'infohash':dict['infohash'],
        'progress_text':`${percentage(completedLength,totalLength)} of ${human_read(totalLength)}`,
        'dspd':`D: ${human_read(downloadSpeed)}/s`,
        'uspd':`U: ${human_read(dict['uploadSpeed'])}/s`,
        'eta':`ETA: ${spendtime(downloadSpeed,completedLength,totalLength)}`,
        'connection':`download from ${dict['connections']} connection.`,
    };
    if(!node){
        node=create_itemnode(gid,dict['cls'],'waiting');
        root.insertBefore(node,root.children[dict['i']]);
    }else{
        let rect=node.getBoundingClientRect();
        if(rect.top>window.innerHeight||rect.top+rect.height<0)
            return 0;
    };
    for(let n of node.getElementsByTagName('div')){
        if(n.id in dictmap){
            n.textContent=dictmap[n.id];
        };
    };
    let progress_bar=node.getElementsByTagName('progress')[0];
    progress_bar.value=completedLength;
    progress_bar.max=totalLength;
    return 0;
};
