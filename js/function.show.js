/*Project aria2-WIOW
 *
 * Copyright (c) 2014 - 2023, multiSnow <https://git.disroot.org/multiSnow>
 *
 *Permission to use, copy, modify, and/or distribute this software for
 *any purpose with or without fee is hereby granted, provided that the
 *above copyright notice and this permission notice appear in all
 *copies.
 *
 *THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 *WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 *AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 *DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 *PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 *TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *PERFORMANCE OF THIS SOFTWARE.
 */

function getversion(){
    return wsreq('version','aria2.getVersion');
};

function getstat(){
    return wsreq('globalstat','aria2.getGlobalStat');
};

function default_option(){
    clear_option_cache('addcache');
    if(document.getElementById('add_with_option').checked){
        shownode(document.getElementById('add_option'));
        return wsreq('default_option','aria2.getGlobalOption');
    }else{
        hidenode(document.getElementById('add_option'));
    };
};

function showactive(){
    return wsreq('showactive','aria2.tellActive',new Array(
        ['gid','status','totalLength','completedLength','uploadLength',
         'downloadSpeed','uploadSpeed','infoHash','numSeeders','pieceLength',
         'numPieces','connections','errorCode','files','bittorrent']));
};

function showstopped(){
    return wsreq('showstopped','aria2.tellStopped',new Array(
        0,parseInt(getattr(document.getElementById('sideinfo'),'data-numstopped')),
        ['gid','status','totalLength','completedLength','uploadLength',
         'downloadSpeed','uploadSpeed','infoHash','numSeeders','pieceLength',
         'numPieces','connections','errorCode','files','bittorrent']));
};

function showwaiting(){
    return wsreq('showwaiting','aria2.tellWaiting',new Array(
        0,parseInt(getattr(document.getElementById('sideinfo'),'data-numwaiting')),
        ['gid','status','totalLength','completedLength','uploadLength',
         'downloadSpeed','uploadSpeed','infoHash','numSeeders','pieceLength',
         'numPieces','connections','errorCode','files','bittorrent']))
};

function globaloption(){
    return wsreq('globaloption','aria2.getGlobalOption');
};

function showoption(gid){
    let artype=getattr(document.getElementById(gid),'data-artype');
    if(artype=='stop'){
        wsreq('showoption_stop','aria2.tellStatus',new Array(String(gid)));
    }else{
        wsreq(`showoption_${artype}_stat`,'aria2.tellStatus',new Array(String(gid)));
        wsreq(`showoption_${artype}_opt`,'aria2.getOption',new Array(String(gid)));
        if(artype=='abtml')
            wsreq(`showoption_${artype}_peer`,'aria2.getPeers',new Array(String(gid)));
    };
    shownode(document.getElementById('showoption'));
    clear_option_cache('singlecache');
    return 0;
};

function hideoption(){
    hidenode(document.getElementById('showoption'));
    document.getElementById('showoption_status').className='';
    document.getElementById('showoption_option').className='';
    clearnode(document.getElementById('showoption_status_announcelist'));
    clearnode(document.getElementById('showoption_statue_peers'));
    clearnode(document.getElementById('showoption_statue_file'));
    return 0;
};

function start_autorefresh(){
    let idfunc_dict={
        'mainactive':showactive,
        'mainstopped':showstopped,
        'mainwaiting':showwaiting
    };
    let sidetags=document.getElementById('sidetags');
    let autorefresh=document.getElementById('autorefresh');
    return setInterval(function(){
        getstat();
        if(autorefresh.checked){
            let func=idfunc_dict[getattr(sidetags,'data-crtshow')];
            if(!!func)func();
        };
    },500);
};
