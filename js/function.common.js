/*Project aria2-WIOW
 *
 * Copyright (c) 2014 - 2023, multiSnow <https://git.disroot.org/multiSnow>
 *
 *Permission to use, copy, modify, and/or distribute this software for
 *any purpose with or without fee is hereby granted, provided that the
 *above copyright notice and this permission notice appear in all
 *copies.
 *
 *THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 *WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 *AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 *DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 *PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 *TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *PERFORMANCE OF THIS SOFTWARE.
 */

function newtag(tagname,root){
    let node=document.createElement(tagname);
    if(root!==undefined)root.appendChild(node);
    return node;
}

function newtxt(text,root){
    let node=document.createTextNode(text);
    if(root!==undefined)root.appendChild(node);
    return node;
}

function clearnode(node){
    while(!!node.lastChild)node.lastChild.remove();
}

function shownode(node){
    if(!node)return;
    return node.classList.remove('hidden');
}

function hidenode(node){
    if(!node)return;
    return node.classList.add('hidden');
}

function setattr(node,attr,value){
    return node.setAttribute(attr,value);
};

function getattr(node,attr){
    return node.getAttribute(attr);
};

function popattr(node,attr){
    let value=getattr(node,attr);
    node.removeAttribute(attr);
    return value;
};

function replace_class(node,oldcls,newcls){
    node.classList.remove(oldcls);
    node.classList.add(newcls);
};

function warning_dialog(string){
    return 0;
};

function topage(page){
    let all_page={'start':'mainstart','active':'mainactive',
                  'stopped':'mainstopped','waiting':'mainwaiting',
                  'info':'maininfo'}
    let sidetagsnode=document.getElementById('sidetags');
    let name=popattr(sidetagsnode,'data-tagshow');
    if(name){
        let tagnode=document.getElementById(name);
        if(tagnode.classList.contains('side_clicked')){
            hidenode(document.getElementById(all_page[name]));
            shownode(tagnode);
            tagnode.classList.remove('side_clicked');
        }else{
            console.log('internal error:',name);
        };
    };
    if(!!page){
        let newtagnode=document.getElementById(page);
        let newpage=all_page[page];
        shownode(document.getElementById(newpage));
        hidenode(newtagnode);
        newtagnode.classList.add('side_clicked');
        setattr(sidetagsnode,'data-crtshow',newpage);
        setattr(sidetagsnode,'data-tagshow',page);
    }else{
        hidenode(document.getElementById(popattr(sidetagsnode,'data-crtshow')));
    }
    return 0;
};

function switch_add_type(input_value){
    let all_type={'uri':'adduri',
                  'torrent':'addtorrent',
                  'metalink':'addmetalink'}
    let add_option=document.getElementById('add_option');
    for(let type in all_type){
        hidenode(document.getElementById(all_type[type]));
        document.getElementById(all_type[type]).value='';
        add_option.classList.remove(type);
    };
    if(!input_value){
        input_value=document.getElementById('add_type_option').elements['add_type'].value;
    }
    let newtype=all_type[input_value];
    setattr(document.getElementById('reset_add'),'onclick',`clearadd('${newtype}')`);
    setattr(document.getElementById('start_add'),'onclick',`add_type_func['${newtype}']()`);
    shownode(document.getElementById(newtype));
    add_option.classList.add(input_value);
    return 0;
}

function clearadd(add){
    document.getElementById(add).value='';
    return 0;
};

function percentage(part,total){
    return `${(part/total*100).toFixed(2)}%`
};

function spendtime(speed,completedsize,totalsize){
    let floor=Math.floor;
    let dtlist=[1,'sec',60,'min',3600,'hour',86400,'day',604300,'week']
    let i=0;
    let time_raw=0;
    speed=parseFloat(speed);
    completedsize=parseFloat(completedsize);
    totalsize=parseFloat(totalsize);
    if(completedsize>=totalsize||speed<=0){
        return '∞';
    }else{
        time_raw=((totalsize-completedsize)/speed).toFixed(0);
    };
    while(i<dtlist.length-2){
        if(time_raw<dtlist[i+2]){
            break;
        }else{
            i+=2;
        };
    };
    return floor(time_raw/dtlist[i])+dtlist[i+1]+((i>1)?floor((time_raw%dtlist[i])/dtlist[i-2])+dtlist[i-1]:'');
};

function human_read(num){
    let type=document.getElementById('type_unit').value=='ice';
    let bignumlist=type?[1,1024,1048576,1073741824,1099511627776,1125899906842624,1152921504606846976,1180591620717411303424,1208925819614629174706176]:[1,1000,1000000,1000000000,1000000000000,1000000000000000,1000000000000000000,1000000000000000000000,1000000000000000000000000];
    let suffixlist=type?['B','KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB']:['B','KB','MB','GB','TB','PB','EB','ZB','YB'];
    num=parseFloat(num);
    for(let i=0;i<suffixlist.length-1;i++){
        if(num<bignumlist[i+1]){
            return `${(num/bignumlist[i]).toFixed(2).valueOf()}${suffixlist[i]}`;
        };
    };
    return `${(num/bignumlist[bignumlist.length-1]).toFixed(2).valueOf()}${suffixlist[suffixlist.length-1]}`;
};

function autoinputsize(node){
    node.size=Math.max(node.value.length/2,1);
};

function setaria2params(){
    let hostname=location.hostname;
    let hashlist=location.hash.substring(1).split('&');
    let hashdata={
        'aria2_host':(!hostname)?'127.0.0.1':hostname,
        'aria2_port':'6800',
        'aria2_token':'',
    };

    for(let [match,key,value] of hashlist.map(n=>n.match(/^([^=]*)=?(.*)$/))){
        hashdata[key]=value;
    };
    document.getElementById('wshost').value=hashdata['aria2_host'];
    document.getElementById('wsport').value=hashdata['aria2_port'];
    document.getElementById('rpctoken').value=hashdata['aria2_token'];
};

function setdirection(){
    let is_horizontal=window.outerWidth>window.outerHeight;
    let direction_del=is_horizontal?'vertical':'horizontal';
    let direction_add=is_horizontal?'horizontal':'vertical'

    // cls may not exists in classList (on startup), thus using add/remove instead of replace
    replace_class(document.body,direction_del,direction_add);
    replace_class(document.getElementById('side'),direction_del,direction_add);
    replace_class(document.getElementById('showoption'),direction_del,direction_add);
};

function onloadfunction(){
    let wss_scheme=document.getElementById('wss_scheme');
    let is_https=location.protocol==='https:';
    close_option(document.getElementById('close_option'));
    wss_scheme.checked=is_https;
    wss_scheme.disabled=is_https;
    setaria2params();
    setdirection();
    return 0;
};
